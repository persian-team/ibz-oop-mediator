package ch.ibz.oop.mediator.hotel

import org.jetbrains.exposed.spring.SpringTransactionManager
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import javax.sql.DataSource

@SpringBootApplication
open class HotelAppBoot {

    @Bean
    open fun transactionManager(dataSource: DataSource) = SpringTransactionManager(dataSource)

}

fun main(args: Array<String>) {
    SpringApplication.run(HotelAppBoot::class.java, *args)
}