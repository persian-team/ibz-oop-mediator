package ch.ibz.oop.mediator.hotel.controllers

import ch.ibz.oop.mediator.hotel.repositories.HotelRepository
import ch.ibz.oop.mediator.shared.controllers.Controller
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RestController

@RestController
class HotelController @Autowired constructor(
        private val hotelRepository: HotelRepository) : Controller() {

    @RequestMapping(value = "hotels", method = arrayOf(GET))
    fun hotels() = hotelRepository.list()

}