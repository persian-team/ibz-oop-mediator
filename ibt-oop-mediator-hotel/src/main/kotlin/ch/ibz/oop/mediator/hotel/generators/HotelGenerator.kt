package ch.ibz.oop.mediator.hotel.generators

import ch.ibz.oop.mediator.hotel.repositories.HotelRepository
import ch.ibz.oop.mediator.shared.generator.Generator
import ch.ibz.oop.mediator.shared.models.Country
import ch.ibz.oop.mediator.shared.models.Models.Hotel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
open class HotelGenerator @Autowired constructor(
        private val hotelRepository: HotelRepository) : Generator<Hotel> {

    @PostConstruct
    fun init() {
        hotelRepository.createTable()

        listOf(
                Pair("Schweizerhof Bern", Country.SWITZERLAND),
                Pair("Emirates Palace, Abu Dhabi", Country.UNITEDARABEMIRATES),
                Pair("Cotton House Hotel", Country.SPAIN),
                Pair("Lime Wood", Country.UNITEDKINGDOM),
                Pair("Ett Hem", Country.SWEDEN),
                Pair("Les Fermes de Marie", Country.FRANCE),
                Pair("The Gritti Palace", Country.ITALY),
                Pair("Blue Radisson", Country.AUSTRIA),
                Pair("Finca Cortesin Hotel Golf & Spa", Country.SPAIN),
                Pair("Qualia", Country.AUSTRALIA),
                Pair("Asaba", Country.JAPAN)
        ).map {
            Hotel(it.first, it.second, randomVacancy(), randomPrice())
        }.forEach { hotelRepository.create(it) }
    }

}