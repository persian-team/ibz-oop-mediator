package ch.ibz.oop.mediator.hotel.repositories

import ch.ibz.oop.mediator.shared.models.Country
import ch.ibz.oop.mediator.shared.models.Models.Hotel
import org.jetbrains.exposed.sql.*
import org.joda.time.DateTime
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional
open class HotelRepository {

    open fun list() = Hotels.selectAll().map { Mappers.hotelMapper(it) }

    open fun create(hotel: Hotel) {
        Hotels.insert {
            it[name] = hotel.name
            it[country] = hotel.country
            it[vacancy] = hotel.vacancy
            it[price] = hotel.price
            it[id] = hotel.id
            it[updated] = DateTime.now()
        }
    }

    open fun createTable() = SchemaUtils.create(Hotels)

    private object Mappers {
        fun hotelMapper(row: ResultRow) = Hotel(
                row[Hotels.name],
                row[Hotels.country],
                row[Hotels.vacancy],
                row[Hotels.price],
                row[Hotels.id],
                row[Hotels.updated].toString())
    }

    private object Hotels : Table() {

        private fun country(name: String) = registerColumn<Country>(name, EnumerationColumnType(Country::class.java))

        val id = uuid("id").primaryKey()
        val name = varchar("name", 100)
        val country = country("country")
        val vacancy = integer("vacancy")
        val price = decimal("price", 10, 2)
        val updated = datetime("updated")

    }

}