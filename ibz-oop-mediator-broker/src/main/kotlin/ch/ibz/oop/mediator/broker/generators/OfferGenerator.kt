package ch.ibz.oop.mediator.broker.generators

import ch.ibz.oop.mediator.broker.BrokerConf
import ch.ibz.oop.mediator.broker.repositories.OfferRepository
import ch.ibz.oop.mediator.shared.models.Models.Flight
import ch.ibz.oop.mediator.shared.models.Models.Hotel
import ch.ibz.oop.mediator.shared.models.Models.Offer
import ch.ibz.oop.mediator.shared.models.Models.OfferReference
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class OfferGenerator @Autowired constructor(
        private val offerRepository: OfferRepository,
        private val msgBroker: SimpMessagingTemplate,
        restTemplateBuilder: RestTemplateBuilder) {

    private val rest = restTemplateBuilder.build()

    @PostConstruct
    fun init() {
        offerRepository.createTable()
    }

    @Scheduled(fixedRate = 5000)
    fun load() {
        val flights = rest.getForObject("http://localhost:8081/flights", Array<Flight>::class.java)
        val hotels = rest.getForObject("http://localhost:8082/hotels", Array<Hotel>::class.java)
        offerRepository.delete(flights.map { it.id }, hotels.map { it.id })
        val offerReferences = offerRepository.references()

        flights.forEach { f ->
            hotels.filter { it.country == f.to.country }
                    .forEach { h ->
                        if (!offerReferences.contains(OfferReference(f.id, h.id))) {
                            val offer = Offer(h.id, h.name, h.country, h.vacancy, h.price, f.id, f.from, f.to, f.vacancy, f.price)
                            offerRepository.create(offer)
                            msgBroker.convertAndSend(BrokerConf.WS_OFFER_QUEUE, offer)
                        }
                    }
        }
    }

}