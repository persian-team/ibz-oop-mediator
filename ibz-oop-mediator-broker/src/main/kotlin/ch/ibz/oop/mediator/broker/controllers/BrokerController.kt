package ch.ibz.oop.mediator.broker.controllers

import ch.ibz.oop.mediator.broker.BrokerConf
import ch.ibz.oop.mediator.broker.repositories.OfferRepository
import ch.ibz.oop.mediator.shared.controllers.Controller
import ch.ibz.oop.mediator.shared.models.Airport
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
open class BrokerController @Autowired constructor(
        private val offerRepository: OfferRepository) : Controller() {

    @CrossOrigin(origins = arrayOf(BrokerConf.ALLOWED_ORIGIN))
    @RequestMapping(value = "offers", method = arrayOf(GET))
    fun offers() = offerRepository.list()

    @CrossOrigin(origins = arrayOf(BrokerConf.ALLOWED_ORIGIN))
    @RequestMapping(value = "offers", params = arrayOf("from", "to"), method = arrayOf(GET))
    fun find(@RequestParam("from") from: Airport,
             @RequestParam("to") to: Airport) = offerRepository.find(from, to)

}