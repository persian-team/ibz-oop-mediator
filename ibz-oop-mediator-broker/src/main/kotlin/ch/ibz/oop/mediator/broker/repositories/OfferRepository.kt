package ch.ibz.oop.mediator.broker.repositories

import ch.ibz.oop.mediator.shared.models.Airport
import ch.ibz.oop.mediator.shared.models.Country
import ch.ibz.oop.mediator.shared.models.Models
import ch.ibz.oop.mediator.shared.models.Models.Offer
import org.jetbrains.exposed.sql.*
import org.joda.time.DateTime
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
@Transactional
open class OfferRepository {

    open fun list() = Offers.selectAll().map { Mappers.offerMapper(it) }

    open fun find(from: Airport, to: Airport) = Offers.select { Offers.flightFrom.eq(from) and Offers.flightTo.eq(to) }
            .map { Mappers.offerMapper(it) }

    open fun references() = Offers.slice(Offers.flightId, Offers.hotelId).selectAll().map { Mappers.offerReferenceMapper(it) }

    open fun create(offer: Offer) {
        Offers.insert {
            it[hotelId] = offer.hotelId
            it[hotelName] = offer.hotelName
            it[hotelCountry] = offer.hotelCountry
            it[hotelVacancy] = offer.hotelVacancy
            it[hotelPrice] = offer.hotelPrice
            it[flightId] = offer.flightId
            it[flightFrom] = offer.flightFrom
            it[flightTo] = offer.flightTo
            it[flightVacancy] = offer.flightVacancy
            it[flightPrice] = offer.flightPrice
            it[totalPrice] = offer.totalPrice
            it[id] = offer.id
            it[updated] = DateTime.now()
        }
    }

    open fun delete(flightUuidList: Collection<UUID>, hotelUuidList: Collection<UUID>) {
        Offers.deleteWhere { Offers.flightId.notInList(flightUuidList) or Offers.hotelId.notInList(hotelUuidList) }
    }

    private object Mappers {
        fun offerMapper(row: ResultRow) = Offer(
                row[Offers.hotelId],
                row[Offers.hotelName],
                row[Offers.hotelCountry],
                row[Offers.hotelVacancy],
                row[Offers.hotelPrice],
                row[Offers.flightId],
                row[Offers.flightFrom],
                row[Offers.flightTo],
                row[Offers.flightVacancy],
                row[Offers.flightPrice],
                row[Offers.totalPrice],
                row[Offers.id],
                row[Offers.updated].toString())

        fun offerReferenceMapper(row: ResultRow) = Models.OfferReference(
                row[Offers.flightId],
                row[Offers.hotelId]
        )
    }

    open fun createTable() = SchemaUtils.create(Offers)

    private object Offers : Table() {

        private fun airport(name: String) = registerColumn<Airport>(name, EnumerationColumnType(Airport::class.java))
        private fun country(name: String) = registerColumn<Country>(name, EnumerationColumnType(Country::class.java))

        val id = uuid("id").primaryKey()
        val hotelId = uuid("hotelId")
        val hotelName = varchar("hotelName", 100)
        val hotelCountry = country("hotelCountry")
        val hotelVacancy = integer("hotelVacancy")
        val hotelPrice = decimal("hotelPrice", 10, 1)
        val flightId = uuid("flightId")
        val flightFrom = airport("flightFrom")
        val flightTo = airport("flightTo")
        val flightVacancy = integer("flightVacancy")
        val flightPrice = decimal("flightPrice", 10, 1)
        val totalPrice = decimal("totalPrice", 10, 1)
        val updated = datetime("updated")

    }

}