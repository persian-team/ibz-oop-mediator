package ch.ibz.oop.mediator.broker

import org.jetbrains.exposed.spring.SpringTransactionManager
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import javax.sql.DataSource

@SpringBootApplication
@EnableScheduling
open class BrokerAppBoot {

    @Bean
    open fun transactionManager(dataSource: DataSource) = SpringTransactionManager(dataSource)

}

object BrokerConf {
    const val ALLOWED_ORIGIN = "http://localhost:4200"
    const val WS_END_POINT = "/ws"
    const val WS_OFFER_QUEUE = "/offer"
}

fun main(args: Array<String>) {
    SpringApplication.run(BrokerAppBoot::class.java, *args)
}