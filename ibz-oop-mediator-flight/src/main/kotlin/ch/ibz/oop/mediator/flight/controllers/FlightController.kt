package ch.ibz.oop.mediator.flight.controllers

import ch.ibz.oop.mediator.flight.repositories.FlightRepository
import ch.ibz.oop.mediator.shared.controllers.Controller
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class FlightController @Autowired constructor(
        private val flightRepository: FlightRepository) : Controller() {

    @RequestMapping(value = "flights", method = arrayOf(GET))
    fun list() = flightRepository.list()

    @RequestMapping(value = "flights/{id}", method = arrayOf(GET))
    fun find(@PathVariable id: UUID) = flightRepository.find(id)

}