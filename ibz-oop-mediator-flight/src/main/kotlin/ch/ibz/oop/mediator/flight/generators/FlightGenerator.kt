package ch.ibz.oop.mediator.flight.generators

import ch.ibz.oop.mediator.flight.repositories.FlightRepository
import ch.ibz.oop.mediator.shared.generator.Generator
import ch.ibz.oop.mediator.shared.models.Airport
import ch.ibz.oop.mediator.shared.models.Models.Flight
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
open class FlightGenerator @Autowired constructor(
        private val flightRepository: FlightRepository) : Generator<Airport> {

    @PostConstruct
    fun init() {
        flightRepository.createTable()
    }

    @Scheduled(fixedRate = 6000)
    fun load() {
        val airports = shuffle(Airport.values())
        flightRepository.create(Flight(airports.first(), airports.last(), randomVacancy(), randomPrice()))
    }

}