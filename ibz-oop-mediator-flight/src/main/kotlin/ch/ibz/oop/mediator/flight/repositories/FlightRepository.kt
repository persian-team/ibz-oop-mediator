package ch.ibz.oop.mediator.flight.repositories

import ch.ibz.oop.mediator.shared.controllers.Controller.ResourceNotFoundException
import ch.ibz.oop.mediator.shared.models.Airport
import ch.ibz.oop.mediator.shared.models.Models.Flight
import org.jetbrains.exposed.sql.*
import org.joda.time.DateTime
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
@Transactional
open class FlightRepository {

    open fun list() = Flights.selectAll().map { Mappers.flightMapper(it) }

    open fun find(id: UUID) = Flights.select { Flights.id.eq(id) }.singleOrNull() ?: throw ResourceNotFoundException("Unknown flight id: $id")

    open fun create(flight: Flight) {
        Flights.insert {
            it[from] = flight.from
            it[to] = flight.to
            it[vacancy] = flight.vacancy
            it[price] = flight.price
            it[id] = flight.id
            it[updated] = DateTime.now()
        }
    }

    open fun createTable() = SchemaUtils.create(Flights)

    private object Mappers {
        fun flightMapper(row: ResultRow) = Flight(
                row[Flights.from],
                row[Flights.to],
                row[Flights.vacancy],
                row[Flights.price],
                row[Flights.id],
                row[Flights.updated].toString())
    }

    private object Flights : Table() {

        private fun airport(name: String) = registerColumn<Airport>(name, EnumerationColumnType(Airport::class.java))

        val id = uuid("id").primaryKey()
        val from = airport("from")
        val to = airport("to")
        val vacancy = integer("vacancy")
        val price = decimal("price", 10, 2)
        val updated = datetime("updated")

    }

}