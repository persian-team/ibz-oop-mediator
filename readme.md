# IBZ | OOP2 - Fallstudie Flugvermittlung

Die Zeiten sind hart für Fluggesellschaften. Billiganbieter drücken auf die Preise, dadurch sinkt die Auslastung in unseren Flugzeugen. Gleichzeitig beklagen sich Hotelbesitzer über leere Betten.

Unser innovatives Konzept könnte hier Abhilfe schaffen.

Wir planen den Aufbau eines verteilten Systems mit welchen die Fluggesellschaften ihre unbelegten Sitze und die Hotels ihre unbelegten Zimmer in Last Minute Angebote umwandeln können, welche anschliessend auf unserer Plattform angeboten resp. verkauft werden.

Wir gehen dabei wie folgt vor:

- Anhand der durch die Hotels eingebrachten freien Zimmer suchen wir bei den Fluggesellschaften nach passenden Flügen und vermerken das in unserer Datenbank.
- Die so erstellten Kombiangebote vermerken wir in unserer Datenbank.
- Diese kann von einem (WinForm) Client abgefragt werden, bei Gefallen können auch Buchungen vorgenommen werden.
- Diese werden bei uns zur Auslösung der Zahlungen verwendet und dienen gleichzeitig dazu, bei den Anbietern die entsprechenden Buchungen resp. Reservationen vorzunehmen.