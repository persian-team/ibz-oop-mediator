import {Component, OnInit} from '@angular/core';
import {Offer} from "../../services/booking-data.service";
import {StompService} from '@stomp/ng2-stompjs';
import 'rxjs/add/operator/map';
import {Message} from '@stomp/stompjs';

@Component({
  selector: 'app-real-time',
  templateUrl: './real-time.component.html',
  styleUrls: ['./real-time.component.css']
})
export class RealTimeComponent implements OnInit {

  private webSocketOfferQueue: string = "/offer";
  offers: Offer[] = [];

  constructor(private stompService: StompService) {
  }

  ngOnInit() {
    this.stompService.subscribe(this.webSocketOfferQueue)
      .map((msg: Message) => msg.body)
      .subscribe((body: string) => {
        this.offers.push(JSON.parse(body));
      });
  }

}
