import {Component, OnInit} from '@angular/core';
import {BookingDataService, Offer, Search} from "../../services/booking-data.service";
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  search: Search = new Search("", "");
  offers: Offer[] = [];

  constructor(private bookingDataService: BookingDataService) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.bookingDataService.findByFlightRoute(this.search).subscribe((offers) => {
      this.offers = offers;
    });
  }

}

