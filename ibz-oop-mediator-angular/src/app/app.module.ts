import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from "@angular/http";
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {SearchComponent} from './components/search/search.component';

import {BookingDataService} from "./services/booking-data.service";
import {StompConfig, StompService} from '@stomp/ng2-stompjs';
import {RealTimeComponent} from './components/real-time/real-time.component';


const appRoutes: Routes = [
  {path: 'realTime', component: RealTimeComponent},
  {path: 'search', component: SearchComponent}
];

const stompConfig: StompConfig = {
  heartbeat_in: 1000,
  heartbeat_out: 1000,
  reconnect_delay: 5000,
  debug: false,
  url: 'ws://localhost:8080/ws',
  headers: {
    login: 'guest',
    passcode: 'guest'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    RealTimeComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [BookingDataService,
    StompService,
    {
      provide: StompConfig,
      useValue: stompConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
