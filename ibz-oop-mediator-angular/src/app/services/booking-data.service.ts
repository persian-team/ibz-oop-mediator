import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class BookingDataService {

  constructor(private http: Http) {
  }

  findOffers() {
    return this.http.get('http://localhost:8080/offers').map(response => response.json());
  }

  findByFlightRoute(search: Search) {
    let params = {params: {from: search.from, to: search.to}};
    console.log(search);
    return this.http.get('http://localhost:8080/offers', params).map(response => response.json());
  }

}

export class Search {
  constructor(public from: String, public to: String) {
  }
}

export interface Offer {
  hotelId: String;
  hotelName: String;
  hotelCountry: String;
  hotelVacancy: Number;
  hotelPrice: String;
  flightId: String;
  flightFrom: String;
  flightTo: String;
  flightVacancy: Number;
  flightPrice: String;
  totalPrice: String;
  id: String;
  updated: String;
}
