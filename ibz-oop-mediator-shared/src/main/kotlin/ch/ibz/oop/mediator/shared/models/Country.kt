package ch.ibz.oop.mediator.shared.models

enum class Country(val displayName: String, val code: String) {

    ANDORRA("Andorra", "AD"),
    UNITEDARABEMIRATES("United Arab Emirates", "AE"),
    AFGHANISTAN("Afghanistan", "AF"),
    ANTIGUAANDBARBUDA("Antigua and Barbuda", "AG"),
    ANGUILLA("Anguilla", "AI"),
    ALBANIA("Albania", "AL"),
    ARMENIA("Armenia", "AM"),
    NETHERLANDSANTILLES("Netherlands Antilles", "AN"),
    ANGOLA("Angola", "AO"),
    ANTARCTICA("Antarctica", "AQ"),
    ARGENTINA("Argentina", "AR"),
    AMERICANSAMOA("American Samoa", "AS"),
    AUSTRIA("Austria", "AT"),
    AUSTRALIA("Australia", "AU"),
    ARUBA("Aruba", "AW"),
    ALANDISLANDS("Åland Islands", "AX"),
    AZERBAIJAN("Azerbaijan", "AZ"),
    BOSNIAANDHERZEGOVINA("Bosnia and Herzegovina", "BA"),
    BARBADOS("Barbados", "BB"),
    BANGLADESH("Bangladesh", "BD"),
    BELGIUM("Belgium", "BE"),
    BURKINAFASO("Burkina Faso", "BF"),
    BULGARIA("Bulgaria", "BG"),
    BAHRAIN("Bahrain", "BH"),
    BURUNDI("Burundi", "BI"),
    BENIN("Benin", "BJ"),
    SAINTBARTHELEMY("Saint Barthélemy", "BL"),
    BERMUDA("Bermuda", "BM"),
    BRUNEI("Brunei", "BN"),
    BOLIVIA("Bolivia", "BO"),
    BONAIRE_SINTEUSTATIUSANDSABA("Bonaire, Sint Eustatius and Saba", "BQ"),
    BRAZIL("Brazil", "BR"),
    BAHAMAS("Bahamas", "BS"),
    BHUTAN("Bhutan", "BT"),
    BOUVETISLAND("Bouvet Island", "BV"),
    BOTSWANA("Botswana", "BW"),
    BELARUS("Belarus", "BY"),
    BELIZE("Belize", "BZ"),
    CANADA("Canada", "CA"),
    COCOSISLANDS("Cocos Islands", "CC"),
    THEDEMOCRATICREPUBLICOFCONGO("The Democratic Republic Of Congo", "CD"),
    CENTRALAFRICANREPUBLIC("Central African Republic", "CF"),
    CONGO("Congo", "CG"),
    SWITZERLAND("Switzerland", "CH"),
    COOKISLANDS("Cook Islands", "CK"),
    CHILE("Chile", "CL"),
    CAMEROON("Cameroon", "CM"),
    CHINA("China", "CN"),
    COLOMBIA("Colombia", "CO"),
    COSTARICA("Costa Rica", "CR"),
    CUBA("Cuba", "CU"),
    CAPEVERDE("Cape Verde", "CV"),
    CURACAO("Curaçao", "CW"),
    CHRISTMASISLAND("Christmas Island", "CX"),
    CYPRUS("Cyprus", "CY"),
    CZECHREPUBLIC("Czech Republic", "CZ"),
    GERMANY("Germany", "DE"),
    DJIBOUTI("Djibouti", "DJ"),
    DENMARK("Denmark", "DK"),
    DOMINICA("Dominica", "DM"),
    DOMINICANREPUBLIC("Dominican Republic", "DO"),
    ALGERIA("Algeria", "DZ"),
    ECUADOR("Ecuador", "EC"),
    ESTONIA("Estonia", "EE"),
    EGYPT("Egypt", "EG"),
    WESTERNSAHARA("Western Sahara", "EH"),
    ERITREA("Eritrea", "ER"),
    SPAIN("Spain", "ES"),
    ETHIOPIA("Ethiopia", "ET"),
    FINLAND("Finland", "FI"),
    FIJI("Fiji", "FJ"),
    FALKLANDISLANDS("Falkland Islands", "FK"),
    MICRONESIA("Micronesia", "FM"),
    FAROEISLANDS("Faroe Islands", "FO"),
    FRANCE("France", "FR"),
    GABON("Gabon", "GA"),
    UNITEDKINGDOM("United Kingdom", "GB"),
    GRENADA("Grenada", "GD"),
    GEORGIA("Georgia", "GE"),
    FRENCHGUIANA("French Guiana", "GF"),
    GUERNSEY("Guernsey", "GG"),
    GHANA("Ghana", "GH"),
    GIBRALTAR("Gibraltar", "GI"),
    GREENLAND("Greenland", "GL"),
    GAMBIA("Gambia", "GM"),
    GUINEA("Guinea", "GN"),
    GUADELOUPE("Guadeloupe", "GP"),
    EQUATORIALGUINEA("Equatorial Guinea", "GQ"),
    GREECE("Greece", "GR"),
    SOUTHGEORGIAANDTHESOUTHSANDWICHISLANDS("South Georgia And The South Sandwich Islands", "GS"),
    GUATEMALA("Guatemala", "GT"),
    GUAM("Guam", "GU"),
    GUINEA_BISSAU("Guinea-Bissau", "GW"),
    GUYANA("Guyana", "GY"),
    HONGKONG("Hong Kong", "HK"),
    HEARDISLANDANDMCDONALDISLANDS("Heard Island And McDonald Islands", "HM"),
    HONDURAS("Honduras", "HN"),
    CROATIA("Croatia", "HR"),
    HAITI("Haiti", "HT"),
    HUNGARY("Hungary", "HU"),
    INDONESIA("Indonesia", "ID"),
    IRELAND("Ireland", "IE"),
    ISRAEL("Israel", "IL"),
    ISLEOFMAN("Isle Of Man", "IM"),
    INDIA("India", "IN"),
    BRITISHINDIANOCEANTERRITORY("British Indian Ocean Territory", "IO"),
    IRAQ("Iraq", "IQ"),
    IRAN("Iran", "IR"),
    ICELAND("Iceland", "IS"),
    ITALY("Italy", "IT"),
    JERSEY("Jersey", "JE"),
    JAMAICA("Jamaica", "JM"),
    JORDAN("Jordan", "JO"),
    JAPAN("Japan", "JP"),
    KENYA("Kenya", "KE"),
    KYRGYZSTAN("Kyrgyzstan", "KG"),
    CAMBODIA("Cambodia", "KH"),
    KIRIBATI("Kiribati", "KI"),
    COMOROS("Comoros", "KM"),
    SAINTKITTSANDNEVIS("Saint Kitts And Nevis", "KN"),
    NORTHKOREA("North Korea", "KP"),
    SOUTHKOREA("South Korea", "KR"),
    KUWAIT("Kuwait", "KW"),
    CAYMANISLANDS("Cayman Islands", "KY"),
    KAZAKHSTAN("Kazakhstan", "KZ"),
    LAOS("Laos", "LA"),
    LEBANON("Lebanon", "LB"),
    SAINTLUCIA("Saint Lucia", "LC"),
    LIECHTENSTEIN("Liechtenstein", "LI"),
    SRILANKA("Sri Lanka", "LK"),
    LIBERIA("Liberia", "LR"),
    LESOTHO("Lesotho", "LS"),
    LITHUANIA("Lithuania", "LT"),
    LUXEMBOURG("Luxembourg", "LU"),
    LATVIA("Latvia", "LV"),
    LIBYA("Libya", "LY"),
    MOROCCO("Morocco", "MA"),
    MONACO("Monaco", "MC"),
    MOLDOVA("Moldova", "MD"),
    MONTENEGRO("Montenegro", "ME"),
    SAINTMARTIN("Saint Martin", "MF"),
    MADAGASCAR("Madagascar", "MG"),
    MARSHALLISLANDS("Marshall Islands", "MH"),
    MACEDONIA("Macedonia", "MK"),
    MALI("Mali", "ML"),
    MYANMAR("Myanmar", "MM"),
    MONGOLIA("Mongolia", "MN"),
    MACAO("Macao", "MO"),
    NORTHERNMARIANAISLANDS("Northern Mariana Islands", "MP"),
    MARTINIQUE("Martinique", "MQ"),
    MAURITANIA("Mauritania", "MR"),
    MONTSERRAT("Montserrat", "MS"),
    MALTA("Malta", "MT"),
    MAURITIUS("Mauritius", "MU"),
    MALDIVES("Maldives", "MV"),
    MALAWI("Malawi", "MW"),
    MEXICO("Mexico", "MX"),
    MALAYSIA("Malaysia", "MY"),
    MOZAMBIQUE("Mozambique", "MZ"),
    NAMIBIA("Namibia", "NA"),
    NEWCALEDONIA("New Caledonia", "NC"),
    NIGER("Niger", "NE"),
    NORFOLKISLAND("Norfolk Island", "NF"),
    NIGERIA("Nigeria", "NG"),
    NICARAGUA("Nicaragua", "NI"),
    NETHERLANDS("Netherlands", "NL"),
    NORWAY("Norway", "NO"),
    NEPAL("Nepal", "NP"),
    NAURU("Nauru", "NR"),
    NIUE("Niue", "NU"),
    NEWZEALAND("New Zealand", "NZ"),
    OMAN("Oman", "OM"),
    PANAMA("Panama", "PA"),
    PERU("Peru", "PE"),
    FRENCHPOLYNESIA("French Polynesia", "PF"),
    PAPUANEWGUINEA("Papua New Guinea", "PG"),
    PHILIPPINES("Philippines", "PH"),
    PAKISTAN("Pakistan", "PK"),
    POLAND("Poland", "PL"),
    SAINTPIERREANDMIQUELON("Saint Pierre And Miquelon", "PM"),
    PITCAIRN("Pitcairn", "PN"),
    PUERTORICO("Puerto Rico", "PR"),
    PALESTINE("Palestine", "PS"),
    PORTUGAL("Portugal", "PT"),
    PALAU("Palau", "PW"),
    PARAGUAY("Paraguay", "PY"),
    QATAR("Qatar", "QA"),
    REUNION("Reunion", "RE"),
    ROMANIA("Romania", "RO"),
    SERBIA("Serbia", "RS"),
    RUSSIA("Russia", "RU"),
    RWANDA("Rwanda", "RW"),
    SAUDIARABIA("Saudi Arabia", "SA"),
    SOLOMONISLANDS("Solomon Islands", "SB"),
    SEYCHELLES("Seychelles", "SC"),
    SUDAN("Sudan", "SD"),
    SWEDEN("Sweden", "SE"),
    SINGAPORE("Singapore", "SG"),
    SAINTHELENA("Saint Helena", "SH"),
    SLOVENIA("Slovenia", "SI"),
    SVALBARDANDJANMAYEN("Svalbard And Jan Mayen", "SJ"),
    SLOVAKIA("Slovakia", "SK"),
    SIERRALEONE("Sierra Leone", "SL"),
    SANMARINO("San Marino", "SM"),
    SENEGAL("Senegal", "SN"),
    SOMALIA("Somalia", "SO"),
    SURINAME("Suriname", "SR"),
    SOUTHSUDAN("South Sudan", "SS"),
    SAOTOMEANDPRINCIPE("Sao Tome And Principe", "ST"),
    ELSALVADOR("El Salvador", "SV"),
    SINTMAARTEN_DUTCHPART("Sint Maarten (Dutch part)", "SX"),
    SYRIA("Syria", "SY"),
    SWAZILAND("Swaziland", "SZ"),
    TURKSANDCAICOSISLANDS("Turks And Caicos Islands", "TC"),
    CHAD("Chad", "TD"),
    FRENCHSOUTHERNTERRITORIES("French Southern Territories", "TF"),
    TOGO("Togo", "TG"),
    THAILAND("Thailand", "TH"),
    TAJIKISTAN("Tajikistan", "TJ"),
    TOKELAU("Tokelau", "TK"),
    TIMOR_LESTE("Timor-Leste", "TL"),
    TURKMENISTAN("Turkmenistan", "TM"),
    TUNISIA("Tunisia", "TN"),
    TONGA("Tonga", "TO"),
    TURKEY("Turkey", "TR"),
    TRINIDADANDTOBAGO("Trinidad and Tobago", "TT"),
    TUVALU("Tuvalu", "TV"),
    TAIWAN("Taiwan", "TW"),
    TANZANIA("Tanzania", "TZ"),
    UKRAINE("Ukraine", "UA"),
    UGANDA("Uganda", "UG"),
    UNITEDSTATESMINOROUTLYINGISLANDS("United States Minor Outlying Islands", "UM"),
    UNITEDSTATES("United States", "US"),
    URUGUAY("Uruguay", "UY"),
    UZBEKISTAN("Uzbekistan", "UZ"),
    VATICAN("Vatican", "VA"),
    SAINTVINCENTANDTHEGRENADINES("Saint Vincent And The Grenadines", "VC"),
    VENEZUELA("Venezuela", "VE"),
    BRITISHVIRGINISLANDS("British Virgin Islands", "VG"),
    U_S_VIRGINISLANDS("U.S. Virgin Islands", "VI"),
    VIETNAM("Vietnam", "VN"),
    VANUATU("Vanuatu", "VU"),
    WALLISANDFUTUNA("Wallis And Futuna", "WF"),
    SAMOA("Samoa", "WS"),
    YEMEN("Yemen", "YE"),
    MAYOTTE("Mayotte", "YT"),
    SOUTHAFRICA("South Africa", "ZA"),
    ZAMBIA("Zambia", "ZM"),
    ZIMBABWE("Zimbabwe", "ZW");

    override fun toString(): String = "$code ($displayName)"

}