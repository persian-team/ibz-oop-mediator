package ch.ibz.oop.mediator.shared.models

import org.joda.time.DateTime
import java.math.BigDecimal
import java.util.*

object Models {

    data class Flight(
            val from: Airport,
            val to: Airport,
            val vacancy: Int,
            val price: BigDecimal,
            val id: UUID = UUID.randomUUID(),
            val updated: String = DateTime.now().toString())

    data class Hotel(
            val name: String,
            val country: Country,
            val vacancy: Int,
            val price: BigDecimal,
            val id: UUID = UUID.randomUUID(),
            val updated: String = DateTime.now().toString())

    data class Offer(
            val hotelId: UUID,
            val hotelName: String,
            val hotelCountry: Country,
            val hotelVacancy: Int,
            val hotelPrice: BigDecimal,
            val flightId: UUID,
            val flightFrom: Airport,
            val flightTo: Airport,
            val flightVacancy: Int,
            val flightPrice: BigDecimal,
            val totalPrice: BigDecimal = hotelPrice + flightPrice,
            val id: UUID = UUID.randomUUID(),
            val updated: String = DateTime.now().toString())

    data class OfferReference(
            val flightId: UUID,
            val hotelId: UUID
    )

}