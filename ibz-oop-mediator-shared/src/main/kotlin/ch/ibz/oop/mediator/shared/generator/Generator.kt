package ch.ibz.oop.mediator.shared.generator

import java.math.BigDecimal
import java.util.*

interface Generator<T> {

    fun shuffle(values: Array<T>): List<T> {
        val v = values.toMutableList()
        Collections.shuffle(v)
        return v
    }

    fun randomVacancy() = Random().nextInt(50)

    fun randomPrice() = BigDecimal((Random().nextFloat() * 2000).toString())

}