package ch.ibz.oop.mediator.shared.controllers

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
open class Controller {

    class ResourceNotFoundException(message: String) : RuntimeException(message)

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException::class)
    open fun noResourceNotFoundException(e: Exception) = e.message

}