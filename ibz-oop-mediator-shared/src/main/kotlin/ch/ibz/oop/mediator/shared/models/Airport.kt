package ch.ibz.oop.mediator.shared.models

enum class Airport(val country: Country, val code: String) {

    ZURICH(Country.SWITZERLAND, "ZHR"),
    GENEVE(Country.SWITZERLAND, "GVA"),
    BARCELONA(Country.SPAIN, "BLA"),
    LONDON_GATWICK(Country.UNITEDKINGDOM, "LGW"),
    LONDON(Country.UNITEDKINGDOM, "LON"),
    ROM(Country.ITALY, "ROM"),
    PARIS(Country.FRANCE, "PAR"),
    VIENNA(Country.AUSTRIA, "VIE"),
    FRANKFURT(Country.GERMANY, "FRA")

}